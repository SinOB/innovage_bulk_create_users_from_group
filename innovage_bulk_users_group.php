<?php

/*
  Plugin Name: Innovage Bulk Create Members For Group
  Plugin URI: https://bitbucket.org/SinOB/innovage_bulk_create_users_from_group
  Description: Allow a buddypress group admin to create user accounts in bulk 
  and join those users to the group as members. 
  Author: Sinead O'Brien
  Version: 1.0
  Author URI: https://bitbucket.org/SinOB
  Requires at least: WP 3.9, BuddyPress 2.0.1
  Tested up to: WP 4.0.1, BuddyPress 2.0.3
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

// Suppress sending of activation email
add_filter('bp_core_signup_send_activation_key', create_function('', 'return false;'));

/** /
 * Embed the necessary javascript and css scripts on a page
 * if they have not already been included.
 */
function innovage_bulk_enqueue_styles_scripts() {
    wp_enqueue_script('innovage-bulk-users-group-js', plugins_url('innovage_bulk_users_group.js', __FILE__));
}

add_action("wp_enqueue_scripts", "innovage_bulk_enqueue_styles_scripts");

/** /
 * Create the user account, set the displayname, year of birth and gender
 * @param type $username
 * @param type $displayname
 * @param type $email_address
 * @param type $password
 * @param type $year_of_birth
 * @param type $gender
 * @return boolean
 */
function innovage_bulk_create_user_account($username, $displayname, $email_address, $password, $year_of_birth, $gender, $group_id) {
    global $wpdb;

    // some input cleanup
    $username = preg_replace('/\s+/', '', sanitize_user($username, true));
    $email_address = sanitize_email($email_address);
    $displayname = sanitize_title($displayname);

    // Create the user meta
    $usermeta = array();
    $usermeta['field_1'] = $displayname;
    $usermeta['password'] = $password;
    $usermeta['nickname'] = $displayname;

    // Create the user via buddypress
    $result = BP_Signup::add_backcompat($username, $password, $email_address, $usermeta);

    if (is_wp_error($result)) {
        return $result;
    } else {
        $new_user_id = $result;

        // set the nickname to be the display name
        update_user_meta($new_user_id, 'nickname', $displayname);

        // Set the new user xprofile year of birth
        xprofile_set_field_data('Year of birth', $new_user_id, $year_of_birth, true);
        // Set the new user xprofile year of birth
        xprofile_set_field_data('Gender', $new_user_id, $gender, true);
        // Need to activate the bp account (this will send email to admin notifing of creation of new user)

        $activation_key = wp_hash($new_user_id);
        update_user_meta($new_user_id, 'activation_key', $activation_key);

        $args = array(
            'user_login' => $username,
            'user_email' => $email_address,
            'activation_key' => $activation_key,
            'meta' => $usermeta,
        );

        BP_Signup::add($args);

        // this should activate the account through buddypress
        // and send admin new user email notification
        apply_filters('bp_core_activate_account', bp_core_activate_signup($activation_key));


        BP_Signup::validate($activation_key);
        // finish activation and set display name appropriately
        // this will activate the account wp account even if for any reason 
        // buddypress activation failed
        $wpdb->query($wpdb->prepare("UPDATE $wpdb->users SET display_name=%s, "
                        . "user_status=0 WHERE ID=%d", $displayname, $new_user_id));

        // Have the member join the group
        groups_accept_invite($new_user_id, $group_id);
        return $new_user_id;
    }
}

add_filter('bp_groups_default_extension', 'innovage_bulk_add_page_to_group');

/** /
 * Add tab and page to group admin to handle bulk adding new users 
 */
function innovage_bulk_add_page_to_group() {
    if (class_exists('BP_Group_Extension')) :

        class Innovage_Bulk_Group_Extension extends BP_Group_Extension {

            function __construct() {
                $args = array(
                    'slug' => 'bulk-add-users',
                    'parent_slug' => 'admin', // URL slug of the parent nav item
                    'nav_item_position' => 100,
                    'screens' => array(
                        'edit' => array(
                            'name' => 'Bulk add new users', // the tab name   
                        ),
                    ),
                );
                parent::init($args);
            }

            function edit_screen($group_id = null) {
                if ($_SERVER['REQUEST_METHOD'] != 'POST') {
                    innovage_bulk_create_users_form();
                }
            }

            function edit_screen_save($group_id = null) {
                //if ($_SERVER['REQUEST_METHOD'] === 'POST' && check_admin_referer('bulk_group_user_create')) {

                $user_fields = array('username', 'displayname', 'email_address', 'password', 'year_of_birth', 'gender');
                $results = array();

                // for each row of user data
                for ($i = 0; $i < count($_POST['username']); $i++) {
                    $row_error = null;
                    foreach ($user_fields as $field) {
                        if (!isset($_POST[$field][$i])) {
                            $_POST[$field][$i] = '';
                        }

                        // sanatise and validate the field
                        $result = sanatise_validate_input($_POST[$field][$i], $field);

                        if (is_wp_error($result)) {
                            $row_error = $result->get_error_message();
                            break;
                        }
                        $_POST[$field][$i] = $result;
                    }
                    // if we have an error - skip to next loop
                    if ($row_error != null) {
                        array_push($results, 'Unable to add account for ' . $_POST['username'][$i] . ' with email (' . $_POST['email_address'][$i] . '). Error:' . $row_error);
                        continue;
                    }

                    // Create the user account and join them to the group
                    $create_result = innovage_bulk_create_user_account(
                            $_POST['username'][$i], $_POST['displayname'][$i], $_POST['email_address'][$i], $_POST['password'][$i], $_POST['year_of_birth'][$i], $_POST['gender'][$i], $group_id);
                    if (is_wp_error($create_result)) {
                        $create_error = $result->get_error_message();
                        array_push($results, 'Unable to add account for (' . $_POST['username'][$i] . ') with email (' . $_POST['email_address'][$i] . '). Error: ' . $create_error);
                        continue;
                    }

                    // Should only get to here if there have been no errors
                    array_push($results, 'Successfully added user ' . $_POST['username'][$i] . ' with email ' . $_POST['email_address'][$i] . " to the system.");
                }
                // display all messages
                bp_core_add_message(implode("\n", $results), 'updated');
                bp_core_redirect(bp_get_group_permalink(groups_get_current_group()) . '/admin/' . $this->slug . '/');
            }

        }

        // end of class
        bp_register_group_extension('Innovage_Bulk_Group_Extension');

    endif;
}

/** /
 * Function to handle validation and sanitation of user input data
 *
 * @param type $value
 * @param type $type
 * @return \WP_Error
 */
function sanatise_validate_input($value, $type) {
    $value = trim($value);
    if (!isset($value) || $value === '') {
        return new WP_Error('broke', __("Missing required field $type!"));
    }
    switch ($type) {
        case 'username':
            if (empty($value) || !validate_username($value)) {
                return new WP_Error('broke', __("Invalid or empty username!"));
            }
            $value = sanitize_user($value);
            if (username_exists($value)) {
                return new WP_Error('broke', __('Sorry, that username already exists!'));
            }
            break;
        case 'displayname':
        case 'password':
            $value = sanitize_text_field($value);
            break;
        case 'email_address':
            if (empty($value) || !is_email($value)) {
                return new WP_Error('broke', __("Invalid or empty email adderss ($value)!"));
            }
            $value = sanitize_email($value);
            if (email_exists($value)) {
                return new WP_Error('broke', __('Sorry, that email address is already used!'));
            }
            break;
        case 'year_of_birth':
            $value = intval($value);
            if (strlen($value) != 4) {
                return new WP_Error('broke', __("Invalid or empty year of birth!"));
            }
            break;
        case 'gender':
            if (!($value === 'Female' || $value === 'Male')) {
                return new WP_Error('broke', __("Invalid gender value!"));
            }
            break;
        default:
            return new WP_Error('broke', __("Invalid data type supplied!"));
    }
    return $value;
}

/** /
 * Display the form to allow group admin to insert details for new users
 */
function innovage_bulk_create_users_form() {
    wp_nonce_field('bulk_group_user_create');
    ?>
    <h2>Bulk add new users to this group</h2>
    <p>This tool will allow you register new users and make those new users members of your group automatically.</p>
    <table id='innovage_bulk_users_table'>
        <tbody>
            <tr>
                <th>Username</th>
                <th>Display name</th>
                <th>Email address</th>
                <th>Password</th>
                <th>Year of birth</th>
                <th>Gender</th>
            </tr>
            <tr><td><input type="text" name="username[]"></td>
                <td><input type="text" name="displayname[]"></td>
                <td><input type="text" name="email_address[]"></td>
                <td><input type="text" name="password[]"></td>
                <td><input type="text" name="year_of_birth[]"></td>
                <td>
                    <select name="gender[]">
                        <option value=""></option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>

                    </select>
                </td></tr>
        </tbody>
    </table>
    <input onclick="addRow(this.form);" type="button" value="Add row" />
    <?php
}
