=== Innovage Bulk Create Members For Group ===
Contributors: SinOB
Tags: buddypress
Requires at least: WP 3.9, BuddyPress 2.0.1
Tested up to: WP 4.0, BuddyPress 2.0.3
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allow a buddypress group administrator to create user accounts in bulk and make these 
users members of the same group automatically. 

== Description ==

Allow the administrator of a buddypress group the ability to add users in bulk to 
the site and automatically add those users as members of the group.

There are currently no admin options.

== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.